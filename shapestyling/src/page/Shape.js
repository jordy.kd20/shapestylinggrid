import React from 'react'

export const Shape = () => {
  return (
    <div className='App-header'>
      <div>
        <div className='float'>
            <div className="rectangle"></div>
            <div className="rectangle-radius"></div>
        </div>
        <div className='float'> 
            <div className="triangle-mid"></div>
            <div className="oval"></div>
        </div>
        <div className="rectangle-radius-height float"></div>
      </div>
      <div>
        <div className='rectangle-radius-foot footer'></div>
      </div>
    </div>
  )
}
